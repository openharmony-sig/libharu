/*Copyright (c) 2022 Huawei Device Co., Ltd.
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/

#include <hpdf.h>
#include <stdio.h>
int main()
{
    HPDF_Doc pdf;
    HPDF_Font font;
    HPDF_Error_Handler error_handler;

    pdf = HPDF_New(error_handler, NULL);
    if (!pdf) {
        printf ("ERROR: cannot create pdf object.\n");
        return 1;
    }

    /* set compression mode */
    HPDF_SetCompressionMode (pdf, HPDF_COMP_ALL);

    /* set page mode to use outlines. */
    HPDF_SetPageMode (pdf, HPDF_PAGE_MODE_USE_OUTLINE);

    /* set password */
    HPDF_SetPassword (pdf, "owner", "user");
    HPDF_Page page;
    page = HPDF_AddPage(pdf);
    font = HPDF_GetFont (pdf, "Helvetica", NULL);
    HPDF_Page_SetWidth (page, 550);
    HPDF_Page_SetHeight (page, 650);
    HPDF_Page_BeginText (page);
    HPDF_Page_SetFontAndSize (page, font, 20);
    HPDF_Page_MoveTextPos (page, 220, HPDF_Page_GetHeight (page) - 70);
    HPDF_Page_ShowText(page, "hello human!");
    HPDF_Page_EndText (page);
    HPDF_SaveToFile(pdf, "test.pdf");
    HPDF_Free(pdf);
    return 0;
}

