# libharu

## 简介
> libharu主要用于生成 PDF格式文件。

![](screenshot/result.gif)

## 下载安装
直接在OpenHarmony-SIG仓中搜索libharu并下载。

## 使用说明
以OpenHarmony 3.1 Beta的rk3568版本为例

1. 库代码存放路径：./third_party/libharu

2. 修改添加依赖的编译脚本，路径：/developtools/bytrace_standard/ohos.build

```
  {
     "subsystem": "developtools",
     "parts": {
       "bytrace_standard": {
         "module_list": [
           "//developtools/bytrace_standard/interfaces/innerkits/native:bytrace_core",
           "//developtools/bytrace_standard/bin:bytrace_target",
           "//developtools/bytrace_standard/bin:bytrace.cfg",
           "//developtools/bytrace_standard/interfaces/kits/js/napi:bytrace",
           "//third_party/libharu:libhpdf",
           "//third_party/libharu:libharu_test"
         ],
         "inner_kits": [
           {
             "type": "so",
             "name": "//developtools/bytrace_standard/interfaces/innerkits/native:bytrace_core",
             "header": {
               "header_files": [
                 "bytrace.h"
               ],
               "header_base": "//developtools/bytrace_standard/interfaces/innerkits/native/include"
             }
           }
         ],
          "test_list": [
           "//developtools/bytrace_standard/bin/test:unittest"
         ]
       }
     }
   }
```

3. 用命令 ./build.sh --product-name rk3568 --ccache 编译

4. 生成库文件路径：out/rk3568/graphic/graphic_standard，该路径会生成库文件

## 接口说明

1. 创建pdf对象
   `HPDF_New(HPDF_Error_Handler user_error_fn, void *user_data)`
   
2. 设置压缩模式
   `HPDF_SetCompressionMode(HPDF_Doc pdf, HPDF_UINT mode)`

3. 设置page模式
   `HPDF_SetPageMode(HPDF_Doc pdf, HPDF_PageMode mode)`

4. 设置密码
   `HPDF_SetPassword(HPDF_Doc pdf, const char *owner_passwd, const char *user_passwd)`

5. 添加pdf到page
   `HPDF_AddPage(HPDF_Doc pdf)`

6. 设置宽度
   `HPDF_Page_SetWidth(HPDF_Page page, HPDF_REAL value)`

7. 设置高度
   `HPDF_Page_SetHeight(HPDF_Page page, HPDF_REAL value)`

8. 设置字体和大小
   `HPDF_Page_SetFontAndSize(HPDF_Page page, HPDF_Font font, HPDF_REAL size)`

9. 移动文字位置
   `HPDF_Page_MoveTextPos(HPDF_Page page, HPDF_REAL x, HPDF_REAL y)`

10. 设置显示的文字
    `HPDF_Page_ShowText(HPDF_Page page, const char *text)`

11. 保存pdf
    `HPDF_Page_ShowText(HPDF_Page page, const char *text)`

12. 回收pdf对象
    `HPDF_Free(HPDF_Doc pdf)`


## 约束与限制

在下述版本验证通过：

DevEco Studio 版本：3.1 Beta1(3.1.0.200)，SDK:API9 Beta5(3.2.10.6)

## 目录结构
````
|---- libharu
|     |---- include         #头文件
|     |---- src             #具体功能实现
|     |---- test            #单元测试用例
|     |---- README.md       #安装使用方法
````

## 贡献代码
使用过程中发现任何问题都可以提 [Issue](https://gitee.com/openharmony-sig/libharu/issues) 给我们，当然，我们也非常欢迎你给我们发 [PR](https://gitee.com/openharmony-sig/libharu/pulls) 。

## 开源协议
本项目基于 [Zlib License](https://gitee.com/openharmony-sig/libharu/blob/master/LICENSE) ，请自由地享受和参与开源。